import pytest
from ..src.case_handlers import *

def get_path(filename):
    return "/home/osirv/pi_labs/Sudar/se_lab7/tests/test_assets/"+filename
    assert error.value.error_code==500

def test_case_file_exist_true():
    """Testiraj za putanju koja postoji"""
    path=get_path("file.html")
    handler = CaseFileNotExistsHandler()
    assert handler.test(path)==False


def test_case_file_exist_false():
    """Testiraj za putanju koja ne postoji"""
    path=get_path("file_a.html")
    handler=CaseFileNotExistsHandler()
    assert handler.test(path)==True

def test_case_file_exist_true_run():
    """Testiraj hoce ga ispravno ucitat"""
    path=get_path("file_a.html")
    handler=CaseFileNotExistsHandler()
    assert handler.test(path)==True

def test_case_file_exist_false_run():
    """Testiraj hoce ga ne ispravno ucitat"""
    path=get_path("file.html")
    handler=CaseFileNotExistsHandler()
    with pytest.raises(CaseError) as error:
        handler.run(path)
    assert error.value.error_code==404